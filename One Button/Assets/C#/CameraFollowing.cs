using UnityEngine;

public class CameraFollowing : MonoBehaviour
{
    public Transform target;
    //public float smoothSpeed = 0.125f;
    public Vector3 offset;

    void LateUpdate()
    {
        transform.position = new Vector3(transform.position.x, target.position.y, -10) + offset;
    }
}

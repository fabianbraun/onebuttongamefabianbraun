using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Pause : MonoBehaviour
{
    public static bool Paused = false;
    public GameObject pausemenuUI;

    // Update is called once per frame

    void Start() 
    {
        pausemenuUI.SetActive(false);
    }
    
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (Paused)
            {
                ResumeGame();
            }
            else
            {
                PauseGame();
            }
        }
    }

    public void ResumeGame ()
    {
        pausemenuUI.SetActive(false);
        Time.timeScale = 1f;
        Paused = false;
    }

    void PauseGame ()
    {
        pausemenuUI.SetActive(true);
        Time.timeScale = 0f;
        Paused = true;
    }

    public void LoadCredits()
    {
        SceneManager.LoadScene("Credits");
    }

    public void QuitGame() 
    {
        Application.Quit();
    }

}

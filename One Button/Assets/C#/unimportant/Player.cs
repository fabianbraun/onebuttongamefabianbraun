using System;
using UnityEngine;

/*
 * Simple Jump
 * */
public class Player : MonoBehaviour
{

    private static Player instance;

    [SerializeField] private LayerMask platformsLayerMask;
    private Rigidbody2D rigidbody2d;
    private BoxCollider2D boxCollider2d;
    private bool waitForStart;
    private bool isDead;


    public Vector3 GetPosition()
    {
        return transform.position;
    }

}
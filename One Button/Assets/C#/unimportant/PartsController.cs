using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PartsController : MonoBehaviour
{
    public Transform[] parts;
    public Vector3 nextPartPosition;
    public GameObject player;
    public float partDrawDistance;
    public float partDeleteDistance;

    void Start()
    {
    //    LoadParts();
    }

    void Update()
    {
        RemoveParts();
        LoadParts();
    }

    void LoadParts()
    {
        while ((nextPartPosition - player.transform.position).y < partDrawDistance)
        {
            Transform part = parts[Random.Range(0, parts.Length)];
            Transform newPart = Instantiate(part, nextPartPosition - part.Find("Startpoint").position, part.rotation, transform);

            nextPartPosition = newPart.Find("Endposition").position;
        }

    }

    void RemoveParts()
    {
        if (transform.childCount>0)
        {
            Transform part = transform.GetChild(0);
            Vector3 diff = player.transform.position = part.position;

                if (diff.y > partDeleteDistance)
            {
                Destroy(part.gameObject);
            }

        
        }

    }

}
